
package puntoylineaApp;

public class Punto {
    public double coordX=0;
    public double coordY=0;

    // Constructor por defecto (0,0)
    public Punto() {
    }
    
    // Constructor que metemos las coordenadas del punto 
    public Punto(double coordX, double coordY) {
        this.coordX = coordX;
        this.coordY = coordY;
    }

    // GETTER Y SETTER
    public double getCoordX() {
        return coordX;
    }

    public void setCoordX(double coordX) {
        this.coordX = coordX;
    }

    public double getCoordY() {
        return coordY;
    }

    public void setCoordY(double coordY) {
        this.coordY = coordY;
    }

}
