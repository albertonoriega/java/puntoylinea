package puntoylineaApp;


public class Linea {
    public Punto puntoA;
    public Punto puntoB;
    public double x1;
    public double x2;
    public double y1;
    public double y2;

    // Constructor por defecto
    public Linea() {
    }

    // Constructor que le metes dos puntos
    public Linea(Punto puntoA, Punto puntoB) {
        this.puntoA = puntoA;
        this.puntoB = puntoB;
    }
    
    // Contructor que le pasas 4 coordenadas y te genera los dos puntos de la linea
    public Linea ( double x1, double y1, double x2, double y2){
        this.puntoA = new Punto(x1,y1);
        this.puntoB = new Punto(x2,y2);
    }
    
    // Método que desplaza a la derecha la línea las unidades pasadas por parametro
    public void mueveDerecha (double moverX){
        
        puntoA.coordX= puntoA.coordX + moverX;
        puntoB.coordX= puntoB.coordX + moverX;
        
    }
    
// Método que desplaza a la izquierda la línea las unidades pasadas por parametro    
    public void mueveIzquierda (double moverX){
        
        puntoA.coordX= puntoA.coordX - moverX;
        puntoB.coordX= puntoB.coordX - moverX;
        
    }
    
    // Método que desplaza arriba la línea las unidades pasadas por parametro
    public void mueveArriba (double moverY){
        
        puntoA.coordY= puntoA.coordY + moverY;
        puntoB.coordY= puntoB.coordY + moverY;
        
    }
    
    // Método que desplaza abajo la línea las unidades pasadas por parametro
    public void mueveAbajo (double moverY){
        
        puntoA.coordY= puntoA.coordY - moverY;
        puntoB.coordY= puntoB.coordY - moverY;
        
    }

    // GETTER Y SETTER
    public Punto getPuntoA() {
        return puntoA;
    }

    public void setPuntoA(Punto puntoA) {
        this.puntoA = puntoA;
    }

    public Punto getPuntoB() {
        return puntoB;
    }

    public void setPuntoB(Punto puntoB) {
        this.puntoB = puntoB;
    }
    
    // Método que te muestra las coordenadas de los dos puntos de la línea
    public String info(){
    
        return "[("+ this.getPuntoA().getCoordX()+","+this.getPuntoA().getCoordY()+"),("+this.getPuntoB().getCoordX()+","+this.getPuntoB().getCoordY()+")]";
    }
}
